import { Injectable } from '@angular/core';
import { Island } from '../entity/island';
import { IslandRelation } from '../entity/island-relation';

@Injectable({
  providedIn: 'root',
})
export class IslandService {
  islands = new Array<Island>();
  islandsRelation = new Array<IslandRelation>();
  cantIsland = 0;
  numberAsigned = 0;
  constructor() {}
}
