import { Island } from './island';

export class IslandRelation {
  origin: Island;
  destination: Island;
}
