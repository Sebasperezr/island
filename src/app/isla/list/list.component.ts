import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IslandService } from 'src/app/services/island.service';
import { AddRelationModalComponent } from '../add-relation-modal/add-relation-modal.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  constructor(
    public islandService: IslandService,
    private modalService: NgbModal
  ) {}
  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }

  pop(element) {
    this.islandService.cantIsland--;
    this.islandService.islands.splice(
      this.islandService.islands.indexOf(element),
      1
    );
  }

  openModal(item) {
    const modalRef = this.modalService.open(AddRelationModalComponent);
    modalRef.componentInstance.origin = item;

    modalRef.result.then((result) => {
      if (result) {
        console.log(result);
      }
    });
  }
}
