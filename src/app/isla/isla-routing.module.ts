import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateComponent } from './create/create.component';
import { IslaComponent } from './isla.component';

const routes: Routes = [
  { path: '', component: IslaComponent },
  {
    path: 'create',
    component: CreateComponent,
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IslaRoutingModule {}
