import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GoIslandsComponent } from './go-islands.component';

describe('GoIslandsComponent', () => {
  let component: GoIslandsComponent;
  let fixture: ComponentFixture<GoIslandsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GoIslandsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GoIslandsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
