import { Component, OnInit } from '@angular/core';
import { Island } from 'src/app/entity/island';
import { IslandRelation } from 'src/app/entity/island-relation';
import { IslandService } from 'src/app/services/island.service';

@Component({
  selector: 'app-go-islands',
  templateUrl: './go-islands.component.html',
  styleUrls: ['./go-islands.component.scss'],
})
export class GoIslandsComponent implements OnInit {
  trip = new Array<Island>();
  island: Array<Island>;

  constructor(private islandService: IslandService) {
    this.island = new Array<Island>();
  }

  solution() {
    this.trip = new Array<Island>();
    this.island = this.islandService.islands;
    let firstIsland = this.island[0];
    this.island.splice(this.island.indexOf(firstIsland), 1);
    this.trip.push(firstIsland);
    this.createTrip(firstIsland);
  }
  createTrip(firstIsland) {
    let nextIslant;
    let relations = this.getRelations(firstIsland);
    relations.forEach((relation) => {
      if (this.island.includes(relation.destination)) {
        nextIslant = relation.destination;
        this.island.splice(this.island.indexOf(nextIslant), 1);
        this.trip.push(nextIslant);
      }
      if (this.island.length > 0) this.createTrip(nextIslant);
    });
  }
  getRelations(island): Array<IslandRelation> {
    return this.islandService.islandsRelation.filter(
      (relation) => relation.origin == island
    );
  }
  ngOnInit(): void {}
}
