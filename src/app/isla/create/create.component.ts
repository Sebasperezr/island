import { Component, OnInit } from '@angular/core';
import { Island } from 'src/app/entity/island';
import { IslandService } from 'src/app/services/island.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
})
export class CreateComponent implements OnInit {
  island: Island;
  cantIsland: number;

  constructor(public islandService: IslandService) {
    this.island = new Island();
    this.cantIsland = 0;
  }

  ngOnInit(): void {}

  createIsland() {
    console.log(this.islandService.islands);
    this.islandService.cantIsland++;
    this.islandService.numberAsigned++;
    this.island.id = this.islandService.numberAsigned;
    this.islandService.islands.push(this.island);
    this.clearIsland();
  }
  clearIsland() {
    this.island = new Island();
  }
}
