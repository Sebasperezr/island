import { Component, OnInit } from '@angular/core';
import { IslandService } from '../services/island.service';

@Component({
  selector: 'app-isla',
  templateUrl: './isla.component.html',
  styleUrls: ['./isla.component.scss'],
})
export class IslaComponent implements OnInit {
  constructor(public islandService: IslandService) {}

  ngOnInit(): void {}
}
