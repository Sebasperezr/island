import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Island } from 'src/app/entity/island';

import { IslandRelation } from 'src/app/entity/island-relation';
import { IslandService } from 'src/app/services/island.service';

@Component({
  selector: 'app-add-relation-modal',
  templateUrl: './add-relation-modal.component.html',
  styleUrls: ['./add-relation-modal.component.scss'],
})
export class AddRelationModalComponent implements OnInit {
  closeResult = '';
  @Input() public origin;
  constructor(
    public activeModal: NgbActiveModal,
    public islandService: IslandService
  ) {}
  ngOnInit(): void {
    console.log('Llegga al modal', this.origin);
  }

  addRelation(item) {
    let relation = new IslandRelation();
    relation.destination = item;
    relation.origin = this.origin;
    this.islandService.islandsRelation.push(relation);
    console.log(this.islandService.islandsRelation);
  }
  pop(item) {
    this.islandService.islandsRelation.splice(
      this.islandService.islandsRelation.indexOf(item),
      1
    );
  }

  passBack() {
    this.activeModal.close();
  }
  close() {
    this.activeModal.close();
  }

  getListIslands(): Array<Island> {
    return this.islandService.islands.filter(
      (relation) => relation.id != this.origin.id
    );
  }
  getListRelation(): Array<IslandRelation> {
    return this.islandService.islandsRelation.filter(
      (relation) => relation.origin == this.origin
    );
  }
}
