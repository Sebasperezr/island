import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IslaRoutingModule } from './isla-routing.module';
import { IslaComponent } from './isla.component';
import { CreateComponent } from './create/create.component';
import { FormsModule } from '@angular/forms';
import { ListComponent } from './list/list.component';
import { AddRelationModalComponent } from './add-relation-modal/add-relation-modal.component';
import { GoIslandsComponent } from './go-islands/go-islands.component';

@NgModule({
  declarations: [IslaComponent, CreateComponent, ListComponent, AddRelationModalComponent, GoIslandsComponent],
  imports: [CommonModule, IslaRoutingModule, FormsModule],
})
export class IslaModule {}
